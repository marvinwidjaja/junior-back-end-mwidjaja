import { suite, test, only, retries } from "mocha-typescript";
import { IUserRepo } from "interfaces/repos";
import UserRepo from "../../components/repos/user_repo";
import UserManager from "../../components/managers/user_manager";
import { IUser } from "interfaces/descriptors";
import { deepStrictEqual, strictEqual } from "assert";
import { IUserManager } from "interfaces/managers";

@suite
class UserManagerTest {
    private userRepo: IUserRepo;
    private userManager: IUserManager;
    private user1: IUser = {
        username: "1",
        name: "1",
        email: "1@gmail.com",
        password: "11111"
    };

    private user2: IUser = {
        username: "2",
        name: "2",
        email: "2@gmail.com",
        password: "11111"
    };

    private user3: IUser = {
        username: "3",
        name: "3",
        email: "3@gmail.com",
        password: "11111"
    };

    private user4: IUser = {
        username: "4",
        name: "4",
        email: "4@gmail.com",
        password: "11111"
    };

    private user5: IUser = {
        username: "5",
        name: "5",
        email: "5@gmail.com",
        password: "11111"
    };

    constructor() {
        this.userRepo = new UserRepo();
        this.userManager = new UserManager(this.userRepo);
    }

    after() {
        this.userRepo.clear();
    }

    @test
    "should be able to register"() {
        const user = this.userManager.register(this.user1);
        deepStrictEqual(user, this.user1);
    }

    @test
    "should throw error when registering user without username, email, or password"() {
        try {
            this.userManager.register({
                email: "e@gmail.com",
                password: "eeeee"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }

        try {
            this.userManager.register({
                username: "e",
                password: "eeeee"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }

        try {
            this.userManager.register({
                username: "e",
                email: "e@gmail.com"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }
    }

    @test
    "should throw error when registering user if another user with the same username exists"() 
    {
        this.userManager.register(this.user1);
        try
        {
            this.userManager.register(this.user1) as any;
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                `Username ${this.user1.username} already exists.`
            );
        }
        
    }

    @test
    "should be able to read user"() 
    {
        this.userManager.register(this.user1);
        let selected = this.userManager.readUser(this.user1.username);
        deepStrictEqual(this.user1, selected);
    }

    @test
    "should throw error when reading user if user not found"() 
    {
        try 
        {
            let selected = this.userManager.readUser(this.user1.username) as any; 
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                `Username ${this.user1.username} not found.`
            );
        }
    }

    @test
    "should be able to delete user"() 
    {
        this.userManager.register(this.user1);
        let del = this.userManager.deleteUser(this.user1.username);
        strictEqual(true, del);
    }

    @test
    "should throw error when deleting user if user not found"()
    {
        this.userManager.register(this.user1);
        try 
        {
            this.userManager.deleteUser(this.user2.username) as any;
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                `Username ${this.user2.username} not found.`
            );
        }
    }

    @test
    "should be able to update user"() 
    {
        this.userManager.register(this.user1);
        const updatedUser = this.userManager.updateUser(this.user1.username, 
        {
            name: "11"
        } as any);
        const user = { ...this.user1 };
        user.name = "11";
        deepStrictEqual(updatedUser, user);
    }

    @test
    "should throw error when updating user if user not found"() 
    {
        this.userManager.register(this.user1);
        try {
            const updatedUser = this.userManager.updateUser(this.user2.username, 
            {
                name: "11"
            } as any);
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                `Username ${this.user2.username} not found.`
                );
        }
    }

    @test
    "should throw error when updating user if user object contains password"() 
    {
        this.userManager.register(this.user1);
        try 
        {
            const updatedUser = this.userManager.updateUser(this.user1.username, 
            {
                password: "hello"
            } as any);
        }
        catch (error)
        {
            strictEqual(
                error.message,
                "Fail to update password"
            );
        }
    }

    @test
    "should be able to update user's password"() 
    {
        this.userManager.register(this.user1);
        const updatedUser = this.userManager.updatePassword(this.user1.username,this.user1.password,"testing") as any;
        const user = { ...this.user1 };
        user.password = "testing";
        deepStrictEqual(updatedUser, user);
    }

    @test
    "should throw error when updating user's password if user not found"() 
    {
        this.userManager.register(this.user1);
        try {
            const updatedUser = this.userManager.updatePassword(this.user2.username,this.user2.password,"qwerty") as any;
        }
        catch (error)
        {
            strictEqual(
                error.message,
                `Username ${this.user2.username} not found.`
                );
        }
    }

    @test
    "should throw error when updating user's password if old password is not the same as new password"() 
    {
        this.userManager.register(this.user1);
        try 
        {
            const updatedUser = this.userManager.updatePassword(this.user1.username,this.user3.password,"qwerty") as any;
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                "Invalid password."
            );
        }
    }

    @test
    "should be able to login"() 
    {
        this.userManager.register(this.user1);
        const login = this.userManager.login(this.user1.username, this.user1.password) as any;
        strictEqual(true, login);
    }

    @test
    "should throw error when login if password is wrong"() 
    {
        this.userManager.register(this.user1);
        try 
        {
            const login = this.userManager.login(this.user1.username,this.user3.password) as any;
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                `Incorrect password for ${this.user1.username}.`
            );
        }
    }
    @test
    "should throw error when login if user does not exist"() 
    {
        this.userManager.register(this.user1);
        try 
        {
            const login = this.userManager.login(this.user2.username,this.user1.password) as any;
        }
        catch (error) 
        {
            strictEqual(
                error.message,
                `Username ${this.user2.username} not found.`
                );
        }
    }

    @test
    "should be able to list user"() 
    {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        const users = this.userManager.listUser({});
        strictEqual(users.data.length, 5);
        deepStrictEqual(users, 
        {
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user1, this.user2, this.user3, this.user4, this.user5]
        });
    }

    @test
    "should be able to filter user when listing user"() 
    {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        
        const users = this.userManager.listUser({ username: "3"});
        strictEqual(users.data.length, 1);
        deepStrictEqual(users, 
        {
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user3]
        });
    }

    @test
    "should be able to sort user when listing user"() 
    {
        this.userManager.register(this.user1);
        this.userManager.register(this.user3);
        this.userManager.register(this.user2);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        
        const users = this.userManager.listUser({}, {sort: {username : 'asc'}});
        strictEqual(users.data.length, 5);
        deepStrictEqual(users, 
        {
            page: 1,
            limit: 10,
            total: 5,
            data:[this.user1,this.user2,this.user3,this.user4,this.user5]
        });
    }

    @test
    "should be able to paginate user when listing user"() 
    {
        this.userManager.register(this.user1);
        this.userManager.register(this.user3);
        this.userManager.register(this.user2);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        
        const users = this.userManager.listUser({}, {page: 2, limit: 8});
        strictEqual(users.data.length, 5);
        deepStrictEqual(users, 
        {
            page: 2,
            limit: 8,
            total: 5,
            data:[this.user1,this.user2,this.user3,this.user4,this.user5]
        });
    }

    @test
    "should be able to list users in descending order"() 
    {
        this.userManager.register(this.user1);
        this.userManager.register(this.user3);
        this.userManager.register(this.user2);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        
        const users = this.userManager.listUser({}, {sort: {username : 'desc'}});
        strictEqual(users.data.length, 5);
        deepStrictEqual(users, 
        {
            page: 1,
            limit: 10,
            total: 5,
            data:[this.user5,this.user4,this.user3,this.user2,this.user1]
        });
    }
}
