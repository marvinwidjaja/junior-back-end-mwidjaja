import { IUserRepo } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";
import * as _ from "lodash";
import { Component } from "merapi";

export default class UserRepo extends Component implements IUserRepo {
    constructor() {
        super();
    }

    private users: { [username: string]: IUser } = {};

    public list(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): IUser[] 
    {
        let res = [];
        if (_.isEmpty(query)) 
        {
            for (const x in this.users) 
            {
                res.push(this.users[x]);  
            }
        }
        else 
        {
            const a: any = query;
            res.push(this.users[a.username]);
        }
        
        if (!_.isEmpty(options)) 
        {
            for (const x in options.sort) 
            {
                res = res.sort(function(obj1, obj2) 
                {
                    var objj1: any = obj1;
                    var objj2: any = obj2
                    if(options.sort[x] === 'asc') 
                    {
                        return objj1[x] - objj2[x] ;
                    }
                    else
                    {
                        return objj2[x] - objj1[x] ;
                    }
                });
            }
            if (options.page || options.limit) 
            {
                return _.chunk(res,options.limit)[options.page-1];
            }
        }
        
        return res;
    }

    public count(query: Object): number 
    {
        if (_.isEmpty(query)) 
        {
            return _.size(this.users);
        }

        else 
        {
            let temp = [];
            const a: any = query;
            let selected = this.read(a);
            if (selected=== null) {
                return 0
            }
            temp.push(selected);
            return _.size(temp);
        }
    }

    public create(object: IUser): IUser 
    {
        const { username } = object;

        if (this.users[username]) 
        {
            return null;
        }

        this.users[username] = object;
        return object;
    }

    public read(username: string): IUser 
    {
        if (this.users[username]) 
        {
            return this.users[username];
        }
        else 
        {
            return null;
        }
    }

    public update(username: string, object: Partial<IUser>): IUser
    {
        let selected = this.read(username);

        if (selected) 
        {
            selected.email = object.email;
            selected.name = object.name;
            selected.password = object.password;
            selected.username = object.username;
            return selected;
        }
        else 
        {
            return null;
        }
    }

    public delete(username: string): boolean 
    {
        let selected = this.read(username);

        if (selected) 
        {
            delete this.users[username];
            return true;
        }
        else 
        {
            return false;
        }
    }

    public clear(): void 
    {
        this.users = {};
    }
}
