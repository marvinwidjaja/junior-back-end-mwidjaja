import { IUserManager } from "interfaces/managers";
import { Component } from "merapi";
import { IUserRepo, Paginated } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";
import * as _ from "lodash";
import UserRepo from "components/repos/user_repo";

export default class UserManager extends Component implements IUserManager {
    constructor(private userRepo: IUserRepo) {
        super();
    }

    public listUser(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): Paginated<IUser> {
        let res = {} as any;
        res['page'] = 1;
        res['limit'] = 10;
        const users = this.userRepo.list({});
        if (_.isEmpty(query)) 
        {
            res['data'] = users;
        }
        else 
        {
            const a: any = query;
            const b = this.readUser(a.username);
            res['data'] = [b];
        }
        if (!_.isEmpty(options)) 
        {
            for (const x in options.sort) 
                {
                res.data = res.data.sort(function(obj1:any, obj2:any) 
                    {
                        if(options.sort[x] === 'asc') 
                    {
                        return obj1[x] - obj2[x] ;
                }
                else 
                {
                        return obj2[x] - obj1[x] ;
                }
            });
        }
            if (options.page)
            {
                res['page'] = options.page;
            }
            if (options.limit) 
            {
                res['limit'] = options.limit;
            }
        }
        res['total'] = this.userRepo.count({});
        return res;
    }

    public descendingUsername(): Paginated<IUser> 
    {
        return this.listUser({}, { sort: { username: "desc" } });
    }

    public readUser(username: string): IUser 
    {
        const user = this.userRepo.read(username);
        if (!user) 
        {
            throw new Error(`Username ${username} not found.`);
        }
        return user;
    }

    public deleteUser(username: string): boolean 
    {
        const user = this.userRepo.delete(username);

        if (!user) 
        {
            throw new Error(`Username ${username} not found.`);
        }

        else 
        {
            return true;
        }


        
    }

    public register(object: IUser): IUser 
    {
        const { username, email, password } = object;

        if (!username || !email || !password) 
        {
            throw new Error("Username, email, and password are required.");
        }

        const user = this.userRepo.create(object);
        if (!user) 
        {
            throw new Error(`Username ${username} already exists.`);
        }
        return user;
    }

    public login(username: string, password: string): boolean 
    {
        const user = this.userRepo.read(username);
        if (!user) 
        {
            throw new Error(`Username ${username} not found.`);
        }
        else 
        {
            if (user.password === password) 
            {
                return true;
            }

            else 
            {
                throw new Error(`Incorrect password for  ${username}.`);      
            }
        }
    }

    public updateUser(username: string, object: IUser): IUser 
    {
        const z = this.userRepo.update(username, object);
        if (object.password) 
        {
            throw new Error("Fail to update password");                  
        }
        else 
        {
            if (z) 
            {
                return z;
            }
            else 
            {
                throw new Error(`Username ${username} not found.`);
                
            }
        }
    }

    public updatePassword(
        username: string,
        oldPassword: string,
        newPassword: string
    ): IUser 
    {
        const user = this.userRepo.read(username);
        if (!user)
        {
            throw new Error(`Username ${username} not found.`);
        }
        else 
        {
            if (user.password === oldPassword) 
            {
                const z = this.userRepo.update(username, {password: newPassword});
                return z;
            }
            else {
                throw new Error("Old password doesn't match");
            }
        }
    }
}
